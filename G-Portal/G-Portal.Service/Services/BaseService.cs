﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using G_Portal.Domain.Models;
using G_Portal.Service.Interfaces;
using G_Portal.DataAccess.Repository;

namespace G_Portal.Service.Services
{
    public class BaseService<T> : IBaseService<T> where T : AbstractEntity
    {
        private readonly IDbContext _context;
        public BaseService(IDbContext context)
        {
            _context = context;
        }

        public long Create(T entity)
        {
            _context.Set<T>().Add(entity);
            _context.SaveChanges();

            return entity.Id;
        }

        public void Delete(long id)
        {
            var entity = _context.Set<T>().Find(id);
            if (entity == null)
            {
                return;
            }

            _context.Set<T>().Remove(entity);
            _context.SaveChanges();
        }

        public List<T> Get()
        {
            return _context.Set<T>().ToList();
        }

        public T GetById(long id)
        {
            return _context.Set<T>().Find(id);
        }

        public IQueryable<T> Select(Expression<Func<T, bool>> whereClause = null)
        {
            return whereClause == null ? _context.Set<T>().AsQueryable() : _context.Set<T>().Where(whereClause).AsQueryable();
        }

        public void Update(T entity)
        {
            _context.Set<T>().Update(entity);
            _context.SaveChanges();
        }
    }
}
