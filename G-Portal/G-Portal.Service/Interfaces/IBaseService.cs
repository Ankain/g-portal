﻿using G_Portal.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace G_Portal.Service.Interfaces
{
    /// <summary>
    /// Base servics interface
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IBaseService<T> where T : AbstractEntity
    {      
        /// <summary>
        /// Select objects
        /// </summary>
        /// <param name="whereClause">condition</param>
        /// <returns>Query</returns>
        IQueryable<T> Select(Expression<Func<T, bool>> whereClause = null);

        /// <summary>
        /// Get object by Id
        /// </summary>
        /// <param name="id">Id</param>
        /// <returns>Object</returns>
        T GetById(long id);

        /// <summary>
        /// Get all objects
        /// </summary>
        /// <returns>List of objects</returns>
        List<T> Get();

        /// <summary>
        /// Create new object
        /// </summary>
        /// <param name="entity">Object</param>
        /// <returns>New object id</returns>
        long Create(T entity);

        /// <summary>
        /// Update object
        /// </summary>
        /// <param name="entity"></param>
        void Update(T entity);

        /// <summary>
        /// Delete object
        /// </summary>
        /// <param name="id"></param>
        void Delete(long id);
    }
}
