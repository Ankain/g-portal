﻿using G_Portal.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace G_Portal.DataAccess.Repository
{
    /// <summary>
    /// Data layer interface
    /// </summary>
    public interface IDbContext
    {
        /// <summary>
        /// Get table with type
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        DbSet<TEntity> Set<TEntity>() where TEntity : class;

        /// <summary>
        /// Articles table
        /// </summary>
        DbSet<Article> Articles { get; set; }

        /// <summary>
        /// Categories table
        /// </summary>
        DbSet<Category> Categories { get; set; }

        /// <summary>
        /// Comments table
        /// </summary>
        DbSet<Comment> Comments { get; set; }

        /// <summary>
        /// Save
        /// </summary>
        void SaveChanges();

        /// <summary>
        /// Save Async
        /// </summary>
        /// <returns></returns>
        Task<int> SaveChangesAsync();
    }
}
