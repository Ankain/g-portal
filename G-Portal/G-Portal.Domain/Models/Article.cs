﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace G_Portal.Domain.Models
{
    public class Article : AbstractEntity
    {
        [Required]
        public string Title { get; set; }

        public string ArticleImg { get; set; }

        [Required]
        public string ShortStory { get; set; }

        public string FullStory { get; set; }

        public bool AllowComments { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime PublishDate { get; set; }

        public bool IsFixed { get; set; }

        public virtual ApplicationUser Author { get; set; }        

        public virtual IEnumerable<Category> Categories { get; set; }

        public virtual IEnumerable<Comment> Comments { get; set; }
    }
}
