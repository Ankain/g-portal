﻿using System.Collections.Generic;

namespace G_Portal.Domain.Models
{
    public class Category : AbstractEntity
    {
        public string Title { get; set; }

        public virtual Category ParentCategory { get; set; }

        public virtual IEnumerable<Category> ChildCategories { get; set; }

        public bool IncludeInMenu { get; set; }
        
    }
}
