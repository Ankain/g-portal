﻿using Microsoft.AspNetCore.Identity;

namespace G_Portal.Domain.Models
{
    public class ApplicationUser : IdentityUser
    {
        public bool IsConfirmed { get; set; }
    }
}
