﻿using G_Portal.Domain.Interfaces;

namespace G_Portal.Domain.Models
{
    /// <summary>
    /// <inheritdoc/>
    /// </summary>
    public class AbstractEntity : IBaseEntity
    {
        public long Id { get; set; }
    }
}
