﻿namespace G_Portal.Domain.Models
{
    public class Comment : AbstractEntity
    {
        public string Text { get; set; }

        public virtual Article Article { get; set; }

        public virtual ApplicationUser Author { get; set; }
    }
}
