﻿namespace G_Portal.Domain.Interfaces
{
    /// <summary>
    /// Base Entity
    /// </summary>
    public interface IBaseEntity
    {
        /// <summary>
        /// Id
        /// </summary>
        long Id { get; set; }
    }
}
